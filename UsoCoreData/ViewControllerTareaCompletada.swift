//
//  ViewControllerTareaCompletada.swift
//  UsoCoreData
//
//  Created by carlos pumayalla on 10/6/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class ViewControllerTareaCompletada: UIViewController {
    
    //var anteriorVC = ViewController()
    @IBAction func completarTarea(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        context.delete(tarea!)
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var tareaLabel: UILabel!
    var tarea:Tarea? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if tarea!.importante {
            tareaLabel.text = "😝\(tarea!.nombre!)"
        }else{
            tareaLabel.text = tarea!.nombre
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
